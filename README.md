# Elementary Cellular automata
one dimensional CA, considering neighbors Left, Mid, Right (-1, 0, 1) giving rise to 2^3=8 unique different neighborhoods and this 2^8 = 256 unique rules.


    python pyscripts/elementary_ca.py

## Cool rules
* 22
* 30
* 60
* 90
* 129
* 150

## Extension with larger neighborhood
Extending the elementary to consider larger neighborhood gives rise to for example (-2, -1, 0, 1, 2) gives rise to huge rule space.

    python pyscripts/eca_extended_neighborhood.py

### Interesting rules with nbr_distance=2 single start
* 3690659980
* 2868658209
* 4180027014


### Interesting rules with nbr_distance=2 random start

* 3689045175
* 3488755522
* 3443909927
* 3406073367
* 2888924855



## Demo
<img src="demos/elementary_ca.gif" alt="drawing" width="750"/>

<img src="demos/elementary_ca_extended.gif" alt="drawing" width="750"/>