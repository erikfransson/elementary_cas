import os
import sys
import pygame
import numpy as np


class ECA:

    alive_color = pygame.Color(255, 255, 255)
    dead_color = pygame.Color(0, 0, 0)
    background_color = pygame.Color(0, 0, 0)

    def __init__(self, size_x, size_y, cell_size=10, rule=30, margin=0.1, start='single'):

        self.size_x = size_x
        self.size_y = size_y
        self.cell_size = cell_size

        self.setup_board(start)
        self.setup_neighbors()
        self.setup_grid(margin)
        self.setup_rules(rule)
        self.y = 0
        self.step = 0

    def setup_board(self, start):
        self.board = {i: 0 for i in range(self.size_x)}
        if start == 'single':
            x_mid = self.size_x // 2
            self.board[x_mid] = 1
        elif start == 'random':
            for xi in range(self.size_x):
                self.board[xi] = np.random.randint(0, 2)

    def setup_neighbors(self):
        self.neighbors = dict()
        for x in range(self.size_x):
            x1 = (x - 1) % self.size_x
            x2 = x
            x3 = (x + 1) % self.size_x
            self.neighbors[x] = [x1, x2, x3]

    def setup_rules(self, rule):
        self.rule = rule
        binary_str = '{:08b}'.format(rule)
        nbr_hoods = [(1, 1, 1), (1, 1, 0), (1, 0, 1), (1, 0, 0), (0, 1, 1), (0, 1, 0), (0, 0, 1), (0, 0, 0)]

        self.update_rule = dict()
        for b, nbr_hood in zip(binary_str, nbr_hoods):
            self.update_rule[nbr_hood] = int(b)

    def _handle_events(self):
        # events
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    return False
        return True

    def setup_grid(self, margin=0.1):
        """ Grid positions used for drawing """
        self.rects = dict()
        for x in range(self.size_x):
            for y in range(self.size_y):
                side = self.cell_size
                x_pos = int((x+margin/2) * side)
                y_pos = int((y+margin/2) * side)
                width = (1 - margin) * side
                r = pygame.rect.Rect(x_pos, y_pos, width, width)
                self.rects[(x, y)] = r

    def run(self, screen, FPS=20, savedir=None, save_data=False):

        # setup
        screen.fill(self.background_color)
        clock = pygame.time.Clock()
        if savedir is not None:
            os.makedirs(savedir, exist_ok=True)
        if save_data:
            self.saved_data = []

        # main loop
        running = True
        while running:

            if not self._handle_events():
                break

            # update game
            self.draw(screen)

            # save snapshot
            if savedir is not None:
                fname = os.path.join(savedir, 'state-{}.png'.format(self.step))
                pygame.image.save(screen, fname)

            if save_data:
                self.saved_data.append(list(self.board.values()))

            self.evolve()
            pygame.display.flip()
            clock.tick(FPS)

            if self.step >= self.size_y:
                running = False

    def evolve(self):
        new_board = dict()
        for x in range(self.size_x):
            nbr_hood = tuple([self.board[xi] for xi in self.neighbors[x]])
            new_board[x] = self.update_rule[nbr_hood]
        self.board = new_board
        self.y += 1
        self.step += 1

    def draw(self, screen, show_step=False, draw_rules=False):
        y = self.y
        for x in range(self.size_x):
            color = self.alive_color if self.board[x] == 1 else self.dead_color
            r = self.rects[(x, y)]
            pygame.draw.rect(screen, color, r, 0)

        if draw_rules:
            self.draw_rules(screen)

    def draw_rules(self, screen):
        color = (102, 2, 60)
        text_str = 'Rule {}'.format(self.rule)
        font = pygame.font.SysFont('dejavuserif', 100, bold=True)
        text = font.render(text_str, True, color)
        screen.blit(text, (50, 30))


if __name__ == '__main__':
    # setup
    pygame.init()
    np.random.seed(42)

    # Constants
    size_x = 1600
    size_y = 900
    FPS = 20
    margin = 0.12

    # setup display
    pygame.init()
    screen = pygame.display.set_mode((size_x, size_y), pygame.SRCALPHA, 32)

    # grid size
    cell_size = 10
    size_xs = size_x / cell_size
    size_ys = size_y / cell_size

    # assert grid perfectly fits into canvas
    assert abs(size_xs - int(size_xs)) < 1e-10
    assert abs(size_ys - int(size_ys)) < 1e-10
    size_xs = int(size_xs)
    size_ys = int(size_ys)

    # game rules
    rule = 90
    start = 'single'

    # run
    ca = ECA(size_xs, size_ys, cell_size, rule, margin, start)
    ca.run(screen, FPS=FPS, savedir='test_v2')
