import pygame
import numpy as np
from elementary_ca import ECA
import itertools
import random


class ECAN(ECA):
    """
    nbr_dist int
    determining how far neighborhood for a cell goes,
    nbr_dist = 1 yields the regular ECA with [-1, 0, 1] or Left Mid Right
    """

    def __init__(self, size_x, size_y, cell_size=10, rule=30, nbr_dist=1, margin=0.1, start='single'):
        self.nbr_dist = nbr_dist
        self.nbr_dirs = sorted(range(-nbr_dist, nbr_dist+1))
        self.n_nbrs = len(self.nbr_dirs)
        super().__init__(size_x, size_y, cell_size, rule, margin, start)

    def setup_neighbors(self):
        self.neighbors = dict()
        for x in range(self.size_x):
            x_nbrs = []
            for offset in self.nbr_dirs:
                x_nbr = (x + offset) % self.size_x
                x_nbrs.append(x_nbr)
            self.neighbors[x] = x_nbrs

    def setup_rules(self, rule):
        self.rule = rule

        n_nbrhoods = 2**self.n_nbrs
        binary_fmt = '{:0' + str(n_nbrhoods) + 'b}'
        binary_str = binary_fmt.format(rule)
        nbr_hoods = list(itertools.product([0, 1], repeat=self.n_nbrs))

        self.update_rule = dict()
        for b, nbr_hood in zip(binary_str, nbr_hoods):
            self.update_rule[nbr_hood] = int(b)

    def draw_rules(self, screen):
        # params
        pos = (30, 25)
        fontsize = 40
        color = (102, 2, 60)

        # draw
        text_str = 'Rule {}'.format(self.rule)
        font = pygame.font.SysFont('dejavuserif', fontsize, bold=True)
        text = font.render(text_str, True, color)
        screen.blit(text, pos)


if __name__ == '__main__':
    # setup
    pygame.init()
    np.random.seed(400)

    # Constants
    size_x = 1600
    size_y = 900
    FPS = 20
    margin = 0

    # setup display
    pygame.init()
    screen = pygame.display.set_mode((size_x, size_y), pygame.SRCALPHA, 32)

    # grid size
    cell_size = 4
    size_xs = size_x / cell_size
    size_ys = size_y / cell_size

    # assert grid perfectly fits into canvas
    assert abs(size_xs - int(size_xs)) < 1e-10
    assert abs(size_ys - int(size_ys)) < 1e-10
    size_xs = int(size_xs)
    size_ys = int(size_ys)

    # game rules
    nbr_distance = 2
    max_rule = 2**(2**(2*nbr_distance+1))
    start = 'random'

    rule = random.randint(0, max_rule)
    rule = 3443909927


    # run single
    ca = ECAN(size_xs, size_ys, cell_size, rule, nbr_distance, margin, start)
    ca.run(screen, savedir='test_v1')
